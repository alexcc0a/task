package com.nesterov;

public class Task {
    public static void main(String[] args) {
        // Задаем матрицу с островами и водой.
        char[][] grid = {

                {'0', '0', '1', '1', '1'},
                {'1', '0', '0', '1', '1'},
                {'0', '0', '0', '0', '0'},
                {'0', '1', '0', '0', '0'},
                {'1', '1', '0', '1', '0'},
                {'0', '0', '0', '1', '1'}
        };

        // Вызываем метод numIsLands для подсчета количества островов.
        int numIslands = numIsLands(grid);
        System.out.println("Number of islands: " + numIslands);
    }

    // Метод для подсчета количества островов.
    public static int numIsLands(char[][] grid) {
        int count = 0;

        // Проходим по всей матрице.
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                // Если находим остров, увеличиваем счетчик и очищаем остальную часть острова.
                if (grid[i][j] == '1') {
                    count++;
                    clearRestOfLand(grid, i, j);
                }
            }
        }
        return count;
    }

    // Метод для очистки остальной части острова.
    private static void clearRestOfLand(char[][] grid, int i, int j) {
        // Проверяем границы матрицы и что текущая ячейка - остров.
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[i].length || grid[i][j] != '1') {
            return;
        }

        // Очищаем текущую ячейку.
        grid[i][j] = '0';

        // Рекурсивно вызываем метод для очистки соседних ячеек.
        clearRestOfLand(grid, i + 1, j);
        clearRestOfLand(grid, i - 1, j);
        clearRestOfLand(grid, i, j + 1);
        clearRestOfLand(grid, i, j - 1);
    }
}


